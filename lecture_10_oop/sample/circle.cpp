#include <circle.hpp>

namespace mt
{
	Circle::Circle()
	{
		std::cout << "Empty constructor" << std::endl;
	}

	Circle::Circle(Point M, int R)
	{
		m_M = M;
		m_R = R;
		if (R <= 0)
		{
			std::cout << "R <= 0" << std::endl;
			m_R = 1;
		}
		std::cout << "Constructor works!" << std::endl;
	}

	Circle::~Circle()
	{
		std::cout << "Destructor works!" << std::endl;
	}

	double Circle::Square()
	{
		return m_R * m_R * PI;
	}

	void Circle::SetR(int R)
	{
		m_R = R;
		if (R <= 0)
		{
			std::cout << "R <= 0" << std::endl;
			m_R = 1;
		}
	}

	void Circle::SetM(Point M)
	{
		m_M.x = M.x;
		m_M.y = M.y;
	}
}